package handlers

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	"bitbucket.org/fbarros/fantasy-cycling/errors"
	"bitbucket.org/fbarros/fantasy-cycling/models"
)

// Response helper used to write reponse
type Response interface {
	JSON() []byte
	StatusCode() int
}

// WriteResponse write the response to http response stream
func WriteResponse(w http.ResponseWriter, res Response) {
	w.WriteHeader(res.StatusCode())
	_, _ = w.Write(res.JSON())
}

// WriteError long the error and write the response to http response stream
func WriteError(w http.ResponseWriter, err error) {
	res, ok := err.(*errors.Error)
	if !ok {
		log.Println(err)
		res = errors.ErrInternal
	}
	WriteResponse(w, res)
}

var jwtSecretKey = []byte(os.Getenv("JWT_SECRET_KEY"))

// CreateJWT func will used to create the JWT while signing in and signing out
func CreateJWT(user models.User) (response string, err error) {
	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &models.Token{
		User: user,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtSecretKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// VerifyJWT func will used to parse the JWT and return its claims
func VerifyJWT(tokenString string) (*models.Token, error) {
	claims := &models.Token{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtSecretKey, nil
	})

	if claims, ok := token.Claims.(*models.Token); ok && token.Valid {
		return claims, nil
	}
	return nil, err
}

// CurrentUser key
const CurrentUser = "currentUser"

// SetToken sets the token and calls RespondJSON
func SetToken(w http.ResponseWriter, token string) {
	authorizationString := "Bearer " + token
	w.Header().Set("Authorization", authorizationString)
}

// SetCurrentUserTokenString generates and sets the token string for the current user
func SetCurrentUserTokenString(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value(CurrentUser).(models.User)
	tokenString, err := CreateJWT(user)
	if err != nil {
		WriteError(w, errors.ErrInternal)
	}
	SetToken(w, tokenString)
}
