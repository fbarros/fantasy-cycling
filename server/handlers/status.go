package handlers

import (
	"net/http"

	"bitbucket.org/fbarros/fantasy-cycling/models"
)

// HealthCheck return 200 ok
func (h *handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	WriteResponse(w, &models.StatusResponseWrapper{Message: "OK"})
}
