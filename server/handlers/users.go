package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/fbarros/fantasy-cycling/errors"
	"bitbucket.org/fbarros/fantasy-cycling/models"
)

// RegisterUser
// Change to make it decode a CreateUser Object instead of decoding to User
func (h *handler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	createUser := &models.CreateUser{}

	json.NewDecoder(r.Body).Decode(&createUser)
	defer r.Body.Close()

	if createUser.Password != createUser.PasswordConfirmation {
		WriteError(w, errors.ErrPasswordConfirmationDoesNotMatch)
		return
	}

	err := createUser.Validate()
	// if err != nil {
	// 	s := []string{}
	// 	for _, err := range err.(validator.ValidationErrors) {
	// 		errorString := "Error:Field validation for '" + err.Field() + "' failed on the '" + err.Tag() + "' tag"
	// 		fmt.Println(errorString)
	// 		s = append(s, errorString)
	// 	}
	// 	response := &models.Response{
	// 		Status:     strings.Join(s, ", "),
	// 		StatusCode: http.StatusBadRequest,
	// 	}
	// 	common.RespondJSON(w, http.StatusBadRequest, response)
	// 	return
	// }

	user, err := createUser.ToUser()
	if err != nil {
		WriteError(w, errors.ErrInternal)
		return
	}

	if err = h.store.RegisterUser(r.Context(), user); err != nil {
		WriteError(w, &errors.Error{Code: http.StatusConflict, Message: err.Error()})
		return
	}
	WriteResponse(w, &models.UserResponseWrapper{User: user})
}

func (h *handler) LoginUser(w http.ResponseWriter, r *http.Request) {
	loginUser := &models.LoginUser{}

	json.NewDecoder(r.Body).Decode(&loginUser)
	defer r.Body.Close()

	user, err := h.store.LoginUser(r.Context(), loginUser)
	if err != nil {
		WriteError(w, err)
		return
	}
	jwtToken, err := CreateJWT(*user)
	if err != nil {
		WriteError(w, errors.ErrInternal)
		return
	}
	SetToken(w, jwtToken)
	WriteResponse(w, &models.UserResponseWrapper{User: user})
}

func (h *handler) GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	username := params["username"]
	if username == "" {
		WriteError(w, errors.ErrInvalidUsername)
		return
	}
	user, err := h.store.GetUser(r.Context(), &models.GetUser{Username: username})
	if err != nil {
		WriteError(w, err)
		return
	}
	SetCurrentUserTokenString(w, r)
	WriteResponse(w, &models.UserResponseWrapper{User: user})
}

func (h *handler) EditUser(w http.ResponseWriter, r *http.Request) {}

func (h *handler) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := h.store.GetUsers(r.Context())
	if err != nil {
		WriteError(w, err)
		return
	}
	SetCurrentUserTokenString(w, r)
	WriteResponse(w, &models.UserResponseWrapper{Users: users})
}
