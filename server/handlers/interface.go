package handlers

import (
	"net/http"

	"bitbucket.org/fbarros/fantasy-cycling/store"
)

// IHandler is implement all the handlers
type IHandler interface {
	HealthCheck(w http.ResponseWriter, r *http.Request)

	RegisterUser(w http.ResponseWriter, r *http.Request)
	LoginUser(w http.ResponseWriter, r *http.Request)
	EditUser(w http.ResponseWriter, r *http.Request)

	GetUser(w http.ResponseWriter, r *http.Request)
	GetUsers(w http.ResponseWriter, r *http.Request)
}

type handler struct {
	store store.IStore
}

// NewHandler return current IHandler implementation
func NewHandler(store store.IStore) IHandler {
	return &handler{store: store}
}
