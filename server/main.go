package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	DBURL := fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=%s", os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"), os.Getenv("DB_SSLMODE"))
	apiPort := fmt.Sprintf(":%s", os.Getenv("API_PORT"))

	args := Args{
		connectionString: DBURL,
		serverPort:       apiPort,
	}

	// run server
	if err := Run(args); err != nil {
		log.Println(err)
	}
}
