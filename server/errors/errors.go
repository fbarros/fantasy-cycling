package errors

import (
	"encoding/json"
	"fmt"
	"net/http"
)

var (
	// ErrInternal HTTP 500
	ErrInternal = &Error{
		Code:    http.StatusInternalServerError,
		Message: "Something went wrong",
	}
	// ErrUnprocessableEntity HTTP 422
	ErrUnprocessableEntity = &Error{
		Code:    http.StatusUnprocessableEntity,
		Message: "Unprocessable Entity",
	}
	// ErrBadRequest HTTP 400
	ErrBadRequest = &Error{
		Code:    http.StatusBadRequest,
		Message: "Error invalid argument",
	}
	// ErrUserNotFound HTTP 404
	ErrUserNotFound = &Error{
		Code:    http.StatusNotFound,
		Message: "User not found",
	}
	// ErrInvalidUsername HTTP 400
	ErrInvalidUsername = &Error{
		Code:    http.StatusBadRequest,
		Message: "Invalid Username",
	}
	// ErrPasswordConfirmationDoesNotMatch HTTP 400
	ErrPasswordConfirmationDoesNotMatch = &Error{
		Code:    http.StatusBadRequest,
		Message: "Password and Password Confirmation fields do not match",
	}
	// ErrMissingJSONWebToken HTTP 403
	ErrMissingJSONWebToken = &Error{
		Code:    http.StatusForbidden,
		Message: "Missing JSON Web Token",
	}
)

// Error main object for error
type Error struct {
	Code    int
	Message string
}

func (err *Error) Error() string {
	return err.String()
}

func (err *Error) String() string {
	if err == nil {
		return ""
	}
	return fmt.Sprintf("error: code=%s message=%s", http.StatusText(err.Code), err.Message)
}

// JSON convert Error in json
func (err *Error) JSON() []byte {
	if err == nil {
		return []byte("{}")
	}
	res, _ := json.Marshal(err)
	return res
}

// StatusCode get status code
func (err *Error) StatusCode() int {
	if err == nil {
		return http.StatusOK
	}
	return err.Code
}
