package main

import (
	"log"
	"net/http"
	"os"

	gorillaHandlers "github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"bitbucket.org/fbarros/fantasy-cycling/handlers"
	"bitbucket.org/fbarros/fantasy-cycling/middleware"
	"bitbucket.org/fbarros/fantasy-cycling/store"
)

// Args struct used to start the application
type Args struct {
	connectionString string
	serverPort       string
}

// Run the server based on given args
func Run(args Args) error {
	// router
	router := mux.NewRouter().PathPrefix("/api/v1/").Subrouter()

	st := store.NewPostgresStore(args.connectionString)
	hnd := handlers.NewHandler(st)
	RegisterAllRoutes(router, hnd)

	// start server
	log.Println("Starting server at port: ", args.serverPort)
	return http.ListenAndServe(args.serverPort, gorillaHandlers.LoggingHandler(os.Stdout, router))
}

// RegisterAllRoutes registers all routes of the api
func RegisterAllRoutes(router *mux.Router, hnd handlers.IHandler) {
	router.Use(middleware.SetHeaders)
	router.HandleFunc("/", hnd.HealthCheck).Methods(http.MethodGet)

	router.HandleFunc("/register", hnd.RegisterUser).Methods(http.MethodPost)
	router.HandleFunc("/login", hnd.LoginUser).Methods(http.MethodPost)

	authenticatedSubroute := router.PathPrefix("/auth").Subrouter()
	authenticatedSubroute.Use(middleware.VerifyJWTAndSetCurrentUser)
	authenticatedSubroute.HandleFunc("/users/{username}", hnd.GetUser).Methods(http.MethodGet)

	adminSubroute := authenticatedSubroute.PathPrefix("/admin").Subrouter()
	adminSubroute.Use(middleware.IsAdmin)
	adminSubroute.HandleFunc("/users", hnd.GetUsers).Methods(http.MethodGet)
}
