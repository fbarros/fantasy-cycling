package store

import (
	"context"

	"bitbucket.org/fbarros/fantasy-cycling/models"
)

// IStore db connection layer
type IStore interface {
	RegisterUser(ctx context.Context, in *models.User) error
	LoginUser(ctx context.Context, in *models.LoginUser) (*models.User, error)

	GetUser(ctx context.Context, in *models.GetUser) (*models.User, error)
	GetUsers(ctx context.Context) ([]*models.User, error)
}
