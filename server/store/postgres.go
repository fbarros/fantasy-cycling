package store

import (
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"bitbucket.org/fbarros/fantasy-cycling/models"
)

type pg struct {
	db *gorm.DB
}

// NewPostgresStore returns a postgres implementation
func NewPostgresStore(conn string) IStore {
	// create database connection
	db, err := gorm.Open(postgres.Open(conn),
		&gorm.Config{
			Logger: logger.New(
				log.New(os.Stdout, "", log.LstdFlags),
				logger.Config{
					LogLevel: logger.Info,
					Colorful: true,
				},
			),
		},
	)
	if err != nil {
		panic("Enable to connect to database: " + err.Error())
	}
	if err := db.AutoMigrate(&models.User{}); err != nil {
		panic("Enable to migrate database: " + err.Error())
	}
	// return store implementation
	return &pg{db: db}
}
