package store

import (
	"context"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"

	"bitbucket.org/fbarros/fantasy-cycling/errors"
	"bitbucket.org/fbarros/fantasy-cycling/models"
)

func (p *pg) RegisterUser(ctx context.Context, in *models.User) error {
	return p.db.WithContext(ctx).Create(&in).Error
}

func (p *pg) LoginUser(ctx context.Context, in *models.LoginUser) (*models.User, error) {
	user, err := p.GetUser(ctx, &models.GetUser{Username: in.Username})

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(in.Password)); err != nil {
		return nil, errors.ErrUserNotFound
	}

	return user, err
}

func (p *pg) GetUser(ctx context.Context, in *models.GetUser) (*models.User, error) {
	user := &models.User{}
	err := p.db.WithContext(ctx).Take(user, models.User{Username: in.Username}).Error
	if err == gorm.ErrRecordNotFound {
		return nil, errors.ErrUserNotFound
	}
	return user, err
}

func (p *pg) GetUsers(ctx context.Context) ([]*models.User, error) {
	users := []*models.User{}

	err := p.db.WithContext(ctx).Find(&users).Error
	return users, err
}
