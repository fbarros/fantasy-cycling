package models

import (
	jwt "github.com/dgrijalva/jwt-go"
)

// Token struct
type Token struct {
	User User `json:"user"`
	*jwt.StandardClaims
}
