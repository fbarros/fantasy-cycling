package models

import (
	"encoding/json"
	"net/http"
)

// StatusResponseWrapper wrapper for status response
type StatusResponseWrapper struct {
	Message string `json:"message,omitempty"`
	Code    int    `json:"-"`
}

// JSON convert UserResponseWrapper in json
func (e *StatusResponseWrapper) JSON() []byte {
	if e == nil {
		return []byte("{}")
	}
	res, _ := json.Marshal(e)
	return res
}

// StatusCode return status code
func (e *StatusResponseWrapper) StatusCode() int {
	if e == nil || e.Code == 0 {
		return http.StatusOK
	}
	return e.Code
}
