package models

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Base struct adding default fields to models
type Base struct {
	UUID      uuid.UUID      `json:"uuid" gorm:"primary_key;not null;"`
	CreatedAt int64          `json:"created_at" gorm:"autocreatetime:milli"`
	UpdatedAt int64          `json:"updated_at" gorm:"autoupdatetime:milli"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index;"`
}

// BeforeCreate set UUID before saving
func (base *Base) BeforeCreate(db *gorm.DB) (err error) {
	base.UUID = uuid.NewV4()
	return
}
