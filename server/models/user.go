package models

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"
	"golang.org/x/crypto/bcrypt"
)

// User struct
type User struct {
	Base

	FirstName string `json:"first_name" gorm:"not null;"`
	LastName  string `json:"last_name" gorm:"not null;"`
	Username  string `json:"username" gorm:"not null;unique;index"`
	Email     string `json:"email" gorm:"not null;unique;index;"`
	Team      string `json:"team" gorm:"not null;unique;"`
	Password  string `json:"password" gorm:"not null;"`
	IsAdmin   bool   `json:"is_admin" gorm:"not null;default:false"`
}

// CreateUser request
type CreateUser struct {
	FirstName            string `json:"first_name" validate:"required,alpha,min=1,max=30"`
	LastName             string `json:"last_name" validate:"required,alpha,min=1,max=30"`
	Username             string `json:"username" validate:"required,alphanum,min=4,max=30"`
	Email                string `json:"email" validate:"required,email"`
	Team                 string `json:"team" validate:"required,alphanum,min=4,max=30"`
	Password             string `json:"password" validate:"required,alphanum,min=8,max=30"`
	PasswordConfirmation string `json:"password_confirmation" validate:"required,alphanum,min=8,max=30"`
}

// Validate user
func (u *CreateUser) Validate() error {
	return validator.New().Struct(u)
}

// ToUser convert CreateUser struct to User
func (u *CreateUser) ToUser() (*User, error) {
	password, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	return &User{
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Username:  u.Username,
		Email:     u.Email,
		Team:      u.Team,
		Password:  string(password),
	}, nil
}

// LoginUser request
type LoginUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// GetUser request
type GetUser struct {
	Username string `json:"username"`
}

// UserResponseWrapper wrapper for the user response
type UserResponseWrapper struct {
	User  *User   `json:"user,omitempty"`
	Users []*User `json:"users,omitempty"`
	Code  int     `json:"-"`
}

// JSON convert UserResponseWrapper in json
func (e *UserResponseWrapper) JSON() []byte {
	if e == nil {
		return []byte("{}")
	}
	res, _ := json.Marshal(e)
	return res
}

// StatusCode return status code
func (e *UserResponseWrapper) StatusCode() int {
	if e == nil || e.Code == 0 {
		return http.StatusOK
	}
	return e.Code
}
