package middleware

import (
	"net/http"

	"bitbucket.org/fbarros/fantasy-cycling/handlers"
	"bitbucket.org/fbarros/fantasy-cycling/models"
)

// IsAdmin Middleware function
func IsAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := r.Context().Value(handlers.CurrentUser).(models.User)

		if user.IsAdmin == false {
			handlers.WriteResponse(w, &models.StatusResponseWrapper{Message: "User doesn't have access to this page", Code: http.StatusForbidden})
		}
		next.ServeHTTP(w, r)
	})
}
