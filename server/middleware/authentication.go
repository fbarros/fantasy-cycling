package middleware

import (
	"context"
	"net/http"
	"strings"

	"bitbucket.org/fbarros/fantasy-cycling/errors"
	"bitbucket.org/fbarros/fantasy-cycling/handlers"
)

// VerifyJWTAndSetCurrentUser verifies the JWT and sets the user in the context
func VerifyJWTAndSetCurrentUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearerToken := r.Header.Get("Authorization")
		var authorizationToken = strings.Split(bearerToken, " ")[1]

		if authorizationToken == "" {
			handlers.WriteError(w, errors.ErrMissingJSONWebToken)
			return
		}

		claims, err := handlers.VerifyJWT(authorizationToken)
		if err != nil {
			handlers.WriteError(w, err)
			return
		}
		ctx := context.WithValue(r.Context(), handlers.CurrentUser, claims.User)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
