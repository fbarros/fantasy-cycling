import React from 'react';

import { Navbar, NavbarBrand, NavbarEnd, NavbarItem, NavbarMenu } from 'bloomer';

export default function Header() {
    return (
        <Navbar>
            <NavbarBrand>
                <NavbarItem href='/'>Fantasy Cycling</NavbarItem>
            </NavbarBrand>
            <NavbarEnd>
                <NavbarMenu>
                    <NavbarItem href='login'>Login</NavbarItem>
                    <NavbarItem href='register'>Register</NavbarItem>
                </NavbarMenu>
            </NavbarEnd>
        </Navbar>
    );
};