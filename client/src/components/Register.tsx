import React from 'react';

import { Columns, Column, Card, CardHeader, CardHeaderTitle, Title, CardContent, Field, Control, Input, Button } from 'bloomer';

export default function Register() {
    return(
        <Columns>
            <Column isSize='1/2' isOffset='1/4'>
                <Card isFullWidth>
                    <CardHeader>
                        <CardHeaderTitle>
                            <Title isSize={5}>Sign In</Title>
                        </CardHeaderTitle>
                    </CardHeader>
                    <CardContent>
                        <Field>
                            <Control>
                                <Input type='text' placeholder='First Name' />
                            </Control>
                        </Field>
                        <Field>
                            <Control>
                                <Input type='text' placeholder='Last Name' />
                            </Control>
                        </Field>
                        <Field>
                            <Control>
                                <Input type='email' placeholder='Email' />
                            </Control>
                        </Field>
                        <Field>
                            <Control>
                                <Input type='text' placeholder='Team Name' />
                            </Control>
                        </Field>
                        <Field>
                            <Control>
                                <Input type='password' placeholder='Password' />
                            </Control>
                        </Field>
                        <Field>
                            <Control>
                                <Input type='password' placeholder='Password Confirmation' />
                            </Control>
                        </Field>
                        <Field isGrouped>
                            <Control>
                                <Button isColor='success'>Register</Button>
                            </Control>
                            <Control>
                                <Button isColor='info'>Login</Button>
                            </Control>
                        </Field>
                    </CardContent>
                </Card>
            </Column>
        </Columns>
    );
};