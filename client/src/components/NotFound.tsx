import React from 'react';


export default function NotFound() {
  return(
    <div>
      <p>Page not found</p>
      <p>Go to the home page →</p>
    </div>
  );
};