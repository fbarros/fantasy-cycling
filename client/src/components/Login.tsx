import React from 'react';

import { Columns, Column, Card, CardHeader, CardHeaderTitle, Title, CardContent, Field, Control, Input, Button } from 'bloomer';

export default function Login() {
  return(
    <Columns>
      <Column isSize='1/2' isOffset='1/4'>
        <Card>
          <CardHeader>
            <CardHeaderTitle>
              <Title isSize={5}>Sign In</Title>
            </CardHeaderTitle>
          </CardHeader>
          <CardContent>
            <Field>
              <Control>
                <Input type='email' placeholder='Email' />
              </Control>
            </Field>
            <Field>
              <Control>
                <Input type='password' placeholder='Password' />
              </Control>
            </Field>
            <Field isGrouped>
              <Control>
                <Button isColor='success'>Login</Button>
              </Control>
              <Control>
                <Button isColor='info'>Register</Button>
              </Control>
            </Field>
          </CardContent>
        </Card>
      </Column>
    </Columns>
  );
};
