import React from 'react';
import { Switch, Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

import { Container } from 'bloomer';

import Foot from './components/Foot'
import Header from './components/Header'
import Home from './components/Home'
import Login from './components/Login'
import NotFound from './components/NotFound'
import Register from './components/Register'

export default function App() {
  return (
    <BrowserRouter>
      <Container>
        <Header />

        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/register' component={Register} />
          <Route component={NotFound} />
        </Switch>
        <Foot />
      </Container>
    </BrowserRouter>
  );
};